﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Item.aspx.cs" Inherits="Shopping.Pages.Item" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <div class="sideBar">
        <h3>Related Items</h3>
    
        <ul class="related">
            <asp:ListView ID="related" runat="server">
                <ItemTemplate>
                    <li>
                        <div class="img">
                            <a href="../Pages/Item.aspx?id=<%# Eval("ID") %>">
                                <img src="../Content/Images/JellyIcon.png" />
                            </a>
                        </div>
                        <div class="info">
                            <a class="title" href="../Pages/Item.aspx?id=<%# Eval("ID") %>"><%# Eval("Name") %></a>
                            <div class="price">
                                <strong>$<%# Eval("Cost") %></strong>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <div class="empty">
                        <p>Seems like there are no related items!</p>
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
        </ul>
    </div>
    <div class="item">
        <h3></h3>
        <ul class="itemContainer">
            <asp:ListView ID="itemSearch" runat="server" OnItemCommand="itemSearch_ItemCommand">
                <ItemTemplate>
                        <div class="img">
                            <img src="../Content/Images/JellyIcon.png" />
                        </div>
                        <div class="info">
                            <h2 class="title"><%# Eval("Name") %></h2>
                            <p><%# Eval("Description") %></p>
                            <div class="price">
                                <span class="st">Price: </span>
                                <strong>$<%# Eval("Cost") %></strong>
                            </div>
                            <div class="quantity">
                                <span class="st">Quantity: </span>
                                <strong><%# Eval("Quantity") %></strong>
                            </div>
                            <div class="actions">
                                <asp:Button ID="cart" runat="server" Text="Add to Cart"  CommandArgument='<%# Eval("ID") %>' CommandName="AddCart"/>
                                <asp:Button ID="wishlist" runat="server" Text="Add to Wishlist" CommandArgument='<%# Eval("ID") %>' CommandName="AddWishlist" />
                            </div>
                        </div>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <div class="empty">
                        <p>Invalid item ID!</p>
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
        </ul>

        <h3>Reviews</h3>
        <ul class="reviewContainer">
            <asp:ListView ID="reviews" runat="server">
                <ItemTemplate>

                </ItemTemplate>
                <EmptyDataTemplate>
                    <div class="empty">
                        <p>There doesn't seem to be any reviews, yet.</p>
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
        </ul>
    </div>
</asp:Content>