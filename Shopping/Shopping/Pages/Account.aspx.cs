﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Shopping.Pages
{
    public partial class Account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("../Default.aspx");

            if(!Page.IsPostBack)
            {
                int id = (int)Session["UserID"];

                List<Pair> parameters = new List<Pair>();
                parameters.Add(new Pair("id", id));

                //List<History> history = DataManager.GetShoppingHistory(parameters);

                User user = DataManager.GetLoginInfo(parameters)[0];

                firstNameText.Text = user.FirstName;
                lastnameText.Text = user.LastName;
                addressText.Text = user.Address;
                zipcodeText.Text = user.Zipcode;
                stateText.Text = user.State;
                emailText.Text = user.Email;
                cityText.Text = user.City;
                phoneText.Text = user.Phone;
            }
        }

        protected void UpdateInfo(object sender, EventArgs e)
        {
            string email = emailText.Text;
            string address = addressText.Text;
            string zipcode = zipcodeText.Text;
            string state = stateText.Text;
            string city = cityText.Text;
            string phone = phoneText.Text;

            List<Pair> parameters = new List<Pair>();
            parameters.Add(new Pair("email", email));
            parameters.Add(new Pair("address", address));
            parameters.Add(new Pair("zipcode", zipcode));
            parameters.Add(new Pair("state", state));
            parameters.Add(new Pair("city", city));
            parameters.Add(new Pair("phone", phone));
            parameters.Add(new Pair("id", (int)Session["UserID"]));

            DataManager.UpdateUser(parameters);
        }


    }
}