﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Title="Login" Inherits="Shopping.Pages.Login" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="loginBox">
        <div class="l-label">
            <h1>Login</h1>
        </div>

        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label>
            </div>
            <asp:TextBox ID="usernameText" runat="server"></asp:TextBox>
        </div>
    
        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
            </div>
            <asp:TextBox ID="passwordText" TextMode="Password" runat="server"></asp:TextBox>
        </div>
    
        <div class="line">
            <asp:Button ID="loginB" runat="server" UseSubmitBehavior="true" Text="Login" OnClick="loginCheck" />
            <div class="l-label">
                <a href="Registration.aspx">Register New Account</a>
            </div>
        </div>

        <div class="l-label">
            <asp:Label CssClass="error" ID="errorL" runat="server" Text=""></asp:Label>
        </div>
    </div>
    
</asp:Content>
