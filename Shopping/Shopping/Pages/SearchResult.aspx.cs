﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Shopping.Pages
{
    public partial class SearchResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                List<Pair> filters = new List<Pair>();

                if (Request.QueryString["Popular"] != null)
                {

                }

                if(Request.QueryString["Category"] != null)
                {
                    filters.Add(new Pair("category", Request.QueryString["Category"]));
                }

                if(Request.QueryString["Keyword"] != null)
                {
                    filters.Add(new Pair("title", "%" + Request.QueryString["Keyword"] + "%" ));
                }

                List<Shopping.Item> items = DataManager.GetItems(filters);

                itemSearch.DataSource = items;
                itemSearch.DataBind();
            }

        }


    }
}