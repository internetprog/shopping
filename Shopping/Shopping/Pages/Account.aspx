﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Account.aspx.cs" Inherits="Shopping.Pages.Account" %>


<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="settings">
        <h3>Account Info</h3>

        <div class="line">
            <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
            <asp:TextBox ID="firstNameText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label8" runat="server" Text="Last Name"></asp:Label>
            <asp:TextBox ID="lastnameText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label2" runat="server" Text="Address"></asp:Label>
            <asp:TextBox ID="addressText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label3" runat="server" Text="Zipcode"></asp:Label>
            <asp:TextBox ID="zipcodeText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label4" runat="server" Text="State"></asp:Label>
            <asp:TextBox ID="stateText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label5" runat="server" Text="Email"></asp:Label>
            <asp:TextBox ID="emailText" TextMode="Email" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label6" runat="server" Text="City"></asp:Label>
            <asp:TextBox ID="cityText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <asp:Label ID="Label7" runat="server" Text="Phone"></asp:Label>
            <asp:TextBox ID="phoneText" runat="server"></asp:TextBox>
        </div>

        <asp:Button ID="Button1" runat="server" Text="Update" OnClick="UpdateInfo" />
    </div>
    

    <div class="settings">
        <h3>Purchase History</h3>

        <asp:ListView ID="history" runat="server">

        </asp:ListView>
    </div>
    
</asp:Content>

