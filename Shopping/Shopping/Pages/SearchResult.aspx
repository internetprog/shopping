﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="Shopping.Pages.SearchResult" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <ul class="itemContainer">
        <asp:ListView ID="itemSearch" runat="server">
            <ItemTemplate>
                <li>
                    <div class="searchImg">
                        <a href="../Pages/Item.aspx?id=<%# Eval("ID") %>">
                            <img src="../Content/Images/JellyIcon.png" />
                        </a>
                    </div>
                    <div class="info">
                        <h3><a class="title" href="../Pages/Item.aspx?id=<%# Eval("ID") %>"><%# Eval("Name") %></a></h3>
                        <p><%# Eval("Description") %></p>
                        <div class="price">
                            <span class="st">Our Price:</span>
                            <strong>$<%# Eval("Cost") %></strong>
                        </div>
                        <div class="actions">
                            <a href="../Pages/Item.aspx?id=<%# Eval("ID") %>">Details</a>
                        </div>
                    </div>
                </li>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div class="empty">
                    <p>Seems like there are no items that fit this criteria!</p>
                </div>
            </EmptyDataTemplate>
        </asp:ListView>
    </ul>
</asp:Content>