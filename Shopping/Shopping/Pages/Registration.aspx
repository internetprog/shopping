﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" MasterPageFile="~/Site.Master" Inherits="Shopping.Pages.Registration" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    
    <div class="loginBox">
        <div class="l-label">
            <h1>Registration</h1>
        </div>
        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label>
            </div>
            <asp:TextBox ID="usernameText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
            </div>
            <asp:TextBox ID="passwordText" TextMode="Password" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label5" runat="server" Text="Re-enter Password"></asp:Label>
            </div>
            <asp:TextBox ID="passwordText2" TextMode="Password" runat="server"></asp:TextBox>
        </div>

    
        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label2" runat="server" Text="First Name"></asp:Label>
            </div>
            <asp:TextBox ID="firstnameText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label3" runat="server" Text="Last Name"></asp:Label>
            </div>
            <asp:TextBox ID="lastnameText" runat="server"></asp:TextBox>
        </div>

        <div class="line">
            <div class="l-label">
                <asp:Label ID="Label6" runat="server" Text="Email"></asp:Label>
            </div>
            <asp:TextBox ID="emailText" TextMode="Email" runat="server"></asp:TextBox>
        </div>
    
        <div class="line">
            <asp:Button ID="loginB" runat="server" Text="Login" OnClick="RegistrationCheck" />
        </div>

    <div class="line">
            <div class="l-label">
                <asp:Label CssClass="error" ID="errorL" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>

    
</asp:Content>