﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Shopping.Pages
{
    public partial class Item : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                if(Request.QueryString["id"] != null)
                {
                    int id = int.Parse(Request.QueryString["id"]);

                    List<Pair> parameters = new List<Pair>();
                    parameters.Add(new Pair("id", id));

                    List<Shopping.Item> item = DataManager.GetItems(parameters);

                    if(item.Count > 0)
                    {
                        parameters.Clear();
                        parameters.Add(new Pair("category", item[0].Category));

                        List<Shopping.Item> relatedItems = DataManager.GetItems(parameters);

                        parameters.Clear();
                        parameters.Add(new Pair("itemid", item[0].ID));

                        List<Review> reviews = DataManager.GetReviews(parameters);

                        related.DataSource = relatedItems;
                        related.DataBind();
                    }

                    itemSearch.DataSource = item;
                    itemSearch.DataBind();
                }
                
            }
        }

        protected void itemSearch_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int argument = int.Parse(e.CommandArgument.ToString());

            if(e.CommandName.ToString().Equals(""))
            {

            }
            else if (e.CommandName.ToString().Equals(""))
            {

            }
        }
    }
}