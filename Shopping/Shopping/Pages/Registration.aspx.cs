﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Shopping.Pages
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RegistrationCheck(object sender, EventArgs e)
        {
            string username = usernameText.Text;
            string firstName = firstnameText.Text;
            string lastName = lastnameText.Text;
            string password = passwordText.Text;
            string password2 = passwordText2.Text;
            string email = emailText.Text;


            errorL.Text = "";
            errorL.Text += (username.Length < 8) ? "Username must be at least 8 characters long!<br />" : "";
            errorL.Text += (firstName.Length <= 0) ? "First name must be at least 1 character long!<br />" : "";
            errorL.Text += (lastName.Length <= 0) ? "Last name must be at least 1 character long!<br />" : "";
            errorL.Text += (password.Length < 8) ? "Password must be at least 8 characters long!<br />" : "";
            errorL.Text += (email.Length <= 0) ? "Must contain an Email<br />" : "";
            errorL.Text += (!Regex.IsMatch(password, "^[a-zA-Z].*")) ? "Password must start with a letter<br />" : "";
            errorL.Text += (!Regex.IsMatch(password, "(?=.*[a-z])")) ? "Password must contain a lowercase letter<br />" : "";
            errorL.Text += (!Regex.IsMatch(password, "(?=.*[A-Z])")) ? "Password must contain an uppercase letter<br />" : "";
            errorL.Text += (!Regex.IsMatch(password, "(?=.*\\d)")) ? "Password must contain a number<br />" : "";
            errorL.Text += (!password.Equals(password2)) ? "Both passwords must match exactly!<br />" : "";


            if(errorL.Text.Length <= 0)
            {
                List<Pair> filters = new List<Pair>();
                filters.Add(new Pair("username", username));
                List<User> userCheck = DataManager.GetLoginInfo(filters);

                filters.Add(new Pair("password", password));
                filters.Add(new Pair("firstname", firstName));
                filters.Add(new Pair("lastname", lastName));
                filters.Add(new Pair("email", email));


                if(userCheck.Count <= 0)
                {
                    int success = DataManager.AddUser(filters);

                    if(success > 0)
                    {
                        errorL.Text = "Account registration succesful!";
                    }
                    else
                    {
                        errorL.Text = "Account registration unsuccessful!";
                    }
                }
                else
                {
                    errorL.Text += "Username is already in use";
                }
            }

        }
    }
}