﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Shopping.Pages
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.DefaultButton = this.loginB.UniqueID;

            if(!Page.IsPostBack)
            {
                Session["UserID"] = null;
                Session["FirstName"] = null;
                Session["LastName"] = null;
                Session["Role"] = null;
            }
            
        }

        protected void loginCheck(object sender, EventArgs e)
        {
            errorL.Text = "";
            string username = usernameText.Text.Trim();
            string password = passwordText.Text.Trim();

            List<Pair> parameters = new List<Pair>();
            parameters.Add(new Pair("username", username));
            parameters.Add(new Pair("password", password));

            List<User> users = DataManager.GetLoginInfo(parameters);

            if(users.Count > 0)
            {
                User user = users[0];

                Session["UserID"] = user.ID;
                Session["FirstName"] = user.FirstName;
                Session["LastName"] = user.LastName;
                Session["Role"] = (user.IsAdmin) ? 1 : 0;
                //Roles should be as follows
                //Admin = 1
                //Registered User = 0
                //General Web User = null

                Response.Redirect("../Default.aspx");
            }
            else
            {
                errorL.Text += "Invalid Username/Password Combination";
            }
        }


    }
}