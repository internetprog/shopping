﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Shopping._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

        <div id="slider"><!-- Defining the main content section -->
        <!-- Promo slider -->
            <section id="slider-wrapper">
                <div class="nivoSlider">
                    <img style="display: none;" src="../Content/Images/Promo1Redo.png" alt="" title="#htmlcaption-1">
                    <img style="display: none;" src="../Content/Images/Promo12.jpg" alt="" title="#htmlcaption-2">
                    <img style="display: none;" src="../Content/Images/Promo13.jpg" alt="" title="#htmlcaption-3">
                </div>
                <div id="htmlcaption-1" class="nivo-html-caption">
                    <h5 class="p2">Welcome to JAM</h5>
                    <p>Shop from the most popular selection of clothing and accessories on the web.</p>
                </div>
                <div id="htmlcaption-2" class="nivo-html-caption">
                    <h5 class="p2">Where you can add any feature products</h5>
                    <p>Put any description here</p>
                </div>
                <div id="htmlcaption-3" class="nivo-html-caption">
                    <h5 class="p2">Or something else</h5>
                    <p>Put any description here</p>
                </div>
            </section>
        </div>

    <div id="main"><!-- Defining submain content section -->
            <section id="content"><!-- Defining the content section #2 -->
                <div id="left">
                    <h3>All Products</h3>
                    <ul>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 1</a>
                                <p>Description</p>
                                <div class="price">
                                    <span class="st">Our price:</span><strong>$550.00</strong>
                                </div>
                                <div class="actions">
                                    <a href="#">Details</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 2</a>
                                <p>Description</p>
                                <div class="price">
                                    <span class="st">Our price:</span><strong>$250.00</strong>
                                </div>
                                <div class="actions">
                                    <a href="#">Details</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 3</a>
                                <p>Description</p>
                                <div class="price">
                                    <span class="st">Our price:</span><strong>$350.00</strong>
                                </div>
                                <div class="actions">
                                    <a href="#">Details</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 4</a>
                                <p>Description</p>
                                <div class="price">
                                    <span class="st">Our price:</span><strong>$550.00</strong>
                                </div>
                                <div class="actions">
                                    <a href="#">Details</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 5</a>
                                <p>Description</p>
                                <div class="price">
                                    <span class="st">Our price:</span><strong>$250.00</strong>
                                </div>
                                <div class="actions">
                                    <a href="#">Details</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 6</a>
                                <p>Description</p>
                                <div class="price">
                                    <span class="st">Our price:</span><strong>$350.00</strong>
                                </div>
                                <div class="actions">
                                    <a href="#">Details</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="right">
                    <h3>Hot Items</h3>
                    <ul>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 7</a>
                                <div class="price">
                                    <span class="usual">$600.00 </span>&nbsp;
                                    <span class="special">$500.00</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 8</a>
                                <div class="price">
                                    <span class="usual">$500.00 </span>&nbsp;
                                    <span class="special">$400.00</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 9</a>
                                <div class="price">
                                    <span class="usual">$700.00 </span>&nbsp;
                                    <span class="special">$600.25</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 10</a>
                                <div class="price">
                                    <span class="usual">$805.00 </span>&nbsp;
                                    <span class="special">$714.25</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 11</a>
                                <div class="price">
                                    <span class="usual">$1205.00 </span>&nbsp;
                                    <span class="special">$1000.25</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img"><a href="#"><img alt="" src="../Content/Images/JellyIcon.png"></a></div>
                            <div class="info">
                                <a class="title" href="#">Product 12</a>
                                <div class="price">
                                    <span class="usual">$200.00 </span>&nbsp;
                                    <span class="special">$190.25</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>

</asp:Content>
