﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Shopping
{
    public static class DataManager
    {
        private static string GetUserDB = "SELECT User_ID, User_Username, User_Password, User_FirstName, User_LastName, " +
                                          " User_Address, User_Zipcode, User_State, User_Email, User_City, User_Phone, " +
                                          " User_IsAdmin" +
                                          " FROM dbo.[User]" +
                                          " WHERE (User_ID = ISNULL(@id, User_ID)) AND (User_Username = ISNULL(@username, User_Username)) " +
                                          " AND (User_Password = ISNULL(@password, User_Password))";

        private static string AddUserDB = "INSERT INTO [dbo].[User]" +
                                          " ([User_Username],[User_Password],[User_FirstName],[User_LastName],[User_Address]" +
                                          " ,[User_Zipcode],[User_State],[User_Email],[User_City],[User_Phone],[User_IsAdmin])" +
                                          " VALUES" +
                                          " (@username,@password,@firstname,@lastname,@address,@zipcode" +
                                          " ,@state,@email,@city,@phone, 0)";

        private static string UpdateUserDB = "UPDATE [dbo].[User]" +
                                             " SET [User_Address] = ISNULL(@address,[User_Address])" +
                                             " ,[User_Zipcode] = ISNULL(@zipcode,[User_Zipcode])" +
                                             " ,[User_State] = ISNULL(@state,[User_State])" +
                                             " ,[User_Email] = ISNULL(@email,[User_Email])" +
                                             " ,[User_City] = ISNULL(@city,[User_City])" +
                                             " ,[User_Phone] = ISNULL(@phone,[User_Phone])" +
                                             " WHERE [User_ID]=@id";

        private static string GetItemsDB = "SELECT Item_ID, Item_Name, Item_Description, Item_Category, Item_Quantity, Item_Cost" +
                                           " FROM dbo.Item" +
                                           " WHERE (Item_Category = ISNULL(@category, Item_Category)) AND (Item_Name LIKE ISNULL(@title, '%%')) AND (Item_ID = ISNULL(@id, Item_ID))";

        private static string GetReviewsDB = "SELECT        Review_ID, Item_ID, User_ID, Review_Rating, Review_Description" +
                                             " FROM            dbo.Review" +
                                             " WHERE        (Item_ID = ISNULL(@itemid, Item_ID)) AND (User_ID = ISNULL(@userid, User_ID)) AND (Review_ID = ISNULL(@id, Review_ID))";

        public static string ConnectionString
        {
            get
            {
                ConnectionStringSettingsCollection connectionStringSettings = ConfigurationManager.ConnectionStrings;
                return connectionStringSettings["DefaultConnection"].ConnectionString;
            }
        }

        /// <summary>
        /// Returns a DataSet from a specified SQL Select command. SQL parameters should be entered before calling this.
        /// </summary>
        /// <param name="command">An Sql Select command to be executed</param>
        /// <returns>A datatable that contains all returned data</returns>
        private static DataSet GetRows(SqlCommand command)
        {
            DataSet data = null;

            try
            {

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    command.Connection = con;

                    adapter.SelectCommand = command;

                    data = new DataSet();
                    adapter.Fill(data);
                }
            }
            catch (Exception ex)
            {

            }

            return data;

        }

        /// <summary>
        /// Executes a query that does not return any data as a result
        /// </summary>
        /// <param name="command">SQL command to execute.</param>
        /// <returns>Integer of the number of affected rows.</returns>
        private static int ExecuteQuery(SqlCommand command)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.Connection = con;

                con.Open();

                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Fills in the parameters of an sql command.
        /// </summary>
        /// <param name="command">SQL command that will be filled with parameter data.</param>
        /// <param name="filters">A list of data that contains the parameter name and its associated data</param>
        /// <returns></returns>
        private static SqlCommand PrepareSQLCommand(string command, List<Pair> filters)
        {

            SqlCommand cmd = new SqlCommand(command);

            cmd.CommandType = CommandType.Text;

            foreach (Pair p in filters)
            {
                string parameterTemp = "@" + p.First;
                cmd.Parameters.AddWithValue(parameterTemp, (p.Second));
            }

            int startPos = 0;
            string[] delimits = { ")", ",", "\n", "\t", };
            while ((startPos = cmd.CommandText.IndexOf('@', startPos)) > 0)
            {
                int endPos = cmd.CommandText.IndexOf(" ", startPos);
                int tempPos = -1;
                for (int i = 0; i < delimits.Length; i++)
                {
                    tempPos = cmd.CommandText.IndexOf(delimits[i], startPos);
                    if ((endPos > tempPos
                        && !(tempPos <= -1))
                        || endPos <= -1)
                    {
                        endPos = tempPos;
                    }
                }

                if (endPos <= -1)
                    endPos = cmd.CommandText.Length;

                string param = cmd.CommandText.Substring(startPos, endPos - startPos);

                startPos = endPos;

                if (!cmd.Parameters.Contains(param))
                    cmd.Parameters.AddWithValue(param, System.DBNull.Value);

            }

            return cmd;
        }

        public static List<User> GetLoginInfo(List<Pair> parameters)
        {
            SqlCommand cmd = PrepareSQLCommand(GetUserDB, parameters);
            DataSet data = GetRows(cmd);

            List<User> users = new List<User>();

            foreach (DataRow dr in data.Tables[0].Rows)
            {
                User user = new User();

                user.Fill(dr);

                users.Add(user);
            }

            return users;
        }

        public static int AddUser(List<Pair> parameters)
        {
            SqlCommand cmd = PrepareSQLCommand(AddUserDB, parameters);

            return ExecuteQuery(cmd);
        }

        public static int UpdateUser(List<Pair> parameters)
        {
            SqlCommand cmd = PrepareSQLCommand(UpdateUserDB, parameters);

            return ExecuteQuery(cmd);
        }

        public static List<Item> GetItems(List<Pair> parameters)
        {
            SqlCommand cmd = PrepareSQLCommand(GetItemsDB, parameters);
            DataSet data = GetRows(cmd);

            List<Item> items = new List<Item>();

            foreach (DataRow dr in data.Tables[0].Rows)
            {
                Item item = new Item();

                item.Fill(dr);

                items.Add(item);
            }

            return items;
        }

        public static List<Review> GetReviews(List<Pair> parameters)
        {
            SqlCommand cmd = PrepareSQLCommand(GetReviewsDB, parameters);
            DataSet data = GetRows(cmd);

            List<Review> reviews = new List<Review>();
            
            foreach (DataRow dr in data.Tables[0].Rows)
            {
                Review review = new Review();

                review.Fill(dr);

                reviews.Add(review);
            }

            return reviews;
        }
    }
}