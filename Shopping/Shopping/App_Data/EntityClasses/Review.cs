﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping
{
    public class Review
    {
        public Review()
        {

        }

        public int ID
        { 
            get; set;
        }

        public int ItemID
        {
            get;
            set;
        }

        public int UserID
        {
            get;
            set;
        }

        public int ReviewRating
        {
            get;
            set;
        }

        public string ReviewDescription
        {
            get;
            set;
        }

        public void Fill(System.Data.DataRow dr)
        {
            if (dr["Review_ID"] != null)
                ID = (int)dr["Review_ID"];

            if (dr["Item_ID"] != null)
                ItemID = (int)dr["Item_ID"];

            if (dr["User_ID"] != null)
                UserID = (int)dr["User_ID"];

            if (dr["Review_Rating"] != null)
                ReviewRating = (int)dr["Review_Rating"];

            if (dr["Review_Description"] != null)
                ReviewDescription = dr["Review_Description"].ToString();
        }
    }
}