﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping
{
    public class Wishlist
    {
        public Wishlist()
        {

        }
        public int ID
        {
            get;
            set;
        }
        public int ItemID
        {
            get;
            set;
        }
        public int Quantity
        {
            get;
            set;
        }
    }
}