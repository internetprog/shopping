﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping
{
    public class Item
    {
        public Item()
        {

        }

        public int ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Category
        {
            get;
            set;
        }

        public float Cost
        {
            get;
            set;
        }

        public int Quantity
        {
            get;
            set;
        }



        public void Fill(System.Data.DataRow dr)
        {
            if (dr["Item_ID"] != null)
                ID = (int)dr["Item_ID"];

            if (dr["Item_Name"] != null)
                Name = dr["Item_Name"].ToString();

            if (dr["Item_Description"] != null)
                Description = dr["Item_Description"].ToString();

            if (dr["Item_Category"] != null)
                Category = (int)dr["Item_Category"];

            if (dr["Item_Cost"] != null)
                Cost = float.Parse(dr["Item_Cost"].ToString());

            if (dr["Item_Quantity"] != null)
                Quantity = (int)dr["Item_Quantity"];
        }
    }
}