﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping
{
    public class History
    {
        public History()
        {

        }

        public int ID
        {
            get;
            set;
        }

        public int UserID
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }
    }
}