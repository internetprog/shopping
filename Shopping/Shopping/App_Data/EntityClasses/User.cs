﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping
{
    public class User
    {
        public User()
        {

        }
        public int ID
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        public bool IsAdmin
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }
        public string Zipcode
        {
            get;
            set;
        }
        public string State
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string City
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }


        public void Fill(System.Data.DataRow dr)
        {
            if (dr["User_ID"] != null)
                this.ID = (int)dr["User_ID"];

            if (dr["User_Username"] != null)
                this.Username = dr["User_Username"].ToString();

            if (dr["User_Password"] != null)
                this.Password = dr["User_Password"].ToString();

            if (dr["User_FirstName"] != null)
                this.FirstName = dr["User_FirstName"].ToString();

            if (dr["User_LastName"] != null)
                this.LastName = dr["User_LastName"].ToString();

            if (dr["User_Address"] != null)
                this.Address = dr["User_Address"].ToString();

            if (dr["User_Zipcode"] != null)
                this.Zipcode = dr["User_Zipcode"].ToString();

            if (dr["User_State"] != null)
                this.State = dr["User_State"].ToString();

            if (dr["User_Email"] != null)
                this.Email = dr["User_Email"].ToString();

            if (dr["User_City"] != null)
                this.City = dr["User_City"].ToString();

            if (dr["User_Phone"] != null)
                this.Phone = dr["User_Phone"].ToString();

            if (dr["User_IsAdmin"] != null)
                this.IsAdmin = (bool)dr["User_IsAdmin"];
        }
    }
}